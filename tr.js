function area() {
    var x11 = document.getElementById("t1");
    var y11 = document.getElementById("t2");
    var x22 = document.getElementById("t3");
    var y22 = document.getElementById("t4");
    var x33 = document.getElementById("t5");
    var y33 = document.getElementById("t6");
    var x44 = document.getElementById("t7");
    var y44 = document.getElementById("t8");
    var x1 = parseFloat(x11.value);
    var y1 = parseFloat(y11.value);
    var x2 = parseFloat(x22.value);
    var y2 = parseFloat(y22.value);
    var x3 = parseFloat(x33.value);
    var y3 = parseFloat(y33.value);
    var x4 = parseFloat(x44.value);
    var y4 = parseFloat(y44.value);
    var a = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
    var b = Math.sqrt(Math.pow((x3 - x2), 2) + Math.pow((y3 - y2), 2));
    var c = Math.sqrt(Math.pow((x3 - x1), 2) + Math.pow((y3 - y1), 2));
    var d = Math.sqrt(Math.pow((x4 - x1), 2) + Math.pow((y4 - y1), 2));
    var e = Math.sqrt(Math.pow((x4 - x2), 2) + Math.pow((y4 - y2), 2));
    var f = Math.sqrt(Math.pow((x3 - x4), 2) + Math.pow((y3 - y4), 2));
    var s = (a + b + c) / 2;
    var s1 = (a + d + e) / 2;
    var s2 = (b + f + e) / 2;
    var s3 = (c + f + d) / 2;
    var a0 = Math.sqrt(s * (s - a) * (s - b) * (s - c));
    var a1 = Math.sqrt(s1 * (s1 - a) * (s1 - d) * (s1 - e)); //Area of triangle formed by first, second,and entered vertex
    var a2 = Math.sqrt(s2 * (s2 - b) * (s2 - f) * (s2 - e)); //Area of triangle formed by third, second,and entered vertex
    var a3 = Math.sqrt(s3 * (s3 - c) * (s3 - f) * (s3 - d)); //Area of triangle formed by first, third,and entered vertex
    console.log(a0);
    console.log(a1 + a2 + a3);
    if (Math.abs(a0 - a1 - a2 - a3) < 0.00000001) //vertex lies inside triangle
     {
        document.getElementById("ans").innerHTML = "The vertex lies inside the triangle";
    }
    else //vertex lies outside triangle
     {
        document.getElementById("ans").innerHTML = "The vertex lies outside the triangle";
    }
    if ((x4 == x1 && y4 == y1) || (x4 == x2 && y4 == y2) || (y4 == y3 && x4 == x3)) //One of the vertex of triangle
     {
        document.getElementById("ans").innerHTML = "It is one of the vertex of triangle";
    }
}
//# sourceMappingURL=tr.js.map